import re
from datetime import datetime
from enum import Enum
from random import randint
from typing import FrozenSet, List, Optional
from uuid import UUID, uuid4

from aiohttp import ClientResponseError
from fastapi import APIRouter, Depends, HTTPException, Response, status, BackgroundTasks
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, Field
from pymongo.errors import PyMongoError

from ..security import Auth, EitherCheck
from ..shared import MongoDB, Proxmox, TaskManager, db, settings
from .bundles import read_bundle
from .templates import read_template

router = APIRouter(prefix="/environments", tags=["Environments"])

collection = db.environments


class StateEnum(str, Enum):
    running = "RUNNING"
    stopped = "STOPPED"
    creating = "CREATING"
    deleting = "DELETING"
    error = "ERROR"


class EnvironmentBase(BaseModel):
    bundle: UUID
    deleted: bool = False
    modified: datetime = Field(default_factory=datetime.now)

    class Config:
        json_schema_extra = {
            "example": {
                "owner": "someone@example.com",
                "bundle": uuid4(),
                "state": "RUNNING",
                "vmids": [100, 200],
            }
        }


class EnvironmentIn(EnvironmentBase):
    created: datetime = Field(default_factory=datetime.now)

    class Config:
        json_schema_extra = {"example": {"owner": "someone@example.com", "bundle": uuid4()}}


class EnvironmentOut(EnvironmentBase):
    id: UUID
    owner: str
    state: StateEnum
    vmids: FrozenSet[int]
    # default for templates created before a created/modified time was stored
    created: datetime = None

    class Config:
        json_schema_extra = {
            "example": {
                "id": uuid4(),
                "owner": "someone@example.com",
                "bundle": uuid4(),
                "state": "RUNNING",
                "vmids": [100, 200],
            }
        }


class EnvironmentUpdate(EnvironmentBase):
    state: Optional[str] = None
    vmids: Optional[FrozenSet[int]] = None
    owner: Optional[str] = None
    bundle: Optional[str] = None
    modified: datetime = Field(default_factory=datetime.now)


async def fetch_environments(
    auto_error: bool = True,
    environment_id: UUID = None,
    bundle_id: str = None,
    owner: str = None,
    auth: Auth = None,
    deleted: bool = False,
    state: str = None,
) -> List[EnvironmentOut]:
    query = {}

    if not deleted or not auth.is_admin:
        query["deleted"] = False
    if environment_id:
        query["id"] = environment_id
    if bundle_id:
        query["bundle"] = bundle_id
    if state:
        query["state"] = state
    elif not query.get("deleted"):
        query["state"] = {"$ne": "DELETING"}

    if "Administrator" not in auth.roles:
        query["owner"] = auth.username
    elif owner:
        query["owner"] = owner

    try:
        result = [environment async for environment in collection.find(query)]
        if auto_error and len(result) == 0:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Environment not found")
    except PyMongoError:
        raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to read DB")

    return result


async def validate_bundle(bundle_id: UUID, auth: Auth):
    try:
        return await read_bundle(bundle_id, auth)
    except HTTPException as e:
        if e.status_code == 404:
            raise HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=f"Invalid bundle: {bundle_id}",
            )
        else:
            raise e


async def create_user(proxmox, username: str):
    auth_realm = settings.PROXMOX_REALM
    user = f"{username}3@{auth_realm}"

    try:
        async with proxmox.get(f"/api2/json/access/users/{user}") as resp:
            existing = (await resp.json())["data"]
    except ClientResponseError as e:
        existing = False

    if not existing:
        async with proxmox.post(f"/api2/json/access/users", json={"userid": user}) as resp:
            pass


async def new_vlan():
    while await collection.find_one({"vlan": (vlan := randint(1, 4000)), "deleted": False}):
        pass
    return vlan


async def new_vmid():
    while await collection.find_one(
        {"vmids": {"$in": [(vmid := randint(settings.PROXMOX_START_ID, settings.PROXMOX_END_ID))]}}
    ):
        pass
    return vmid


async def clone_vm(proxmox, t, new_id):
    async with proxmox.post(
        f"/api2/json/nodes/{settings.PROXMOX_NODE}/qemu/{t['vmid']}/clone",
        json={
            "newid": new_id,
            "pool": settings.PROXMOX_POOL,
            "description": " ",
            "name": t["name"],
        },
    ) as resp:
        upid = (await resp.json())["data"]

    await proxmox.wait(upid)


async def set_perms(proxmox, association, user, new_id):
    full_user = f"{user}@{settings.PROXMOX_REALM}"
    async with proxmox.put(
        "/api2/json/access/acl",
        json={
            "path": f"/vms/{new_id}",
            "roles": association["user_role"],
            "users": [full_user],
        },
    ) as _:
        pass


async def update_network(proxmox, new_id, vlan):
    async with proxmox.get(
        f"/api2/json/nodes/{settings.PROXMOX_NODE}/qemu/{new_id}/config"
    ) as resp:
        res = (await resp.json())["data"]
    net_dev = [n for n in res.keys() if n.startswith("net")]
    if len(net_dev) == 0:
        return
    updated = {}
    for dev in net_dev:
        updated[dev] = re.sub("bridge=\w+", f"bridge={settings.PROXMOX_BRIDGE}", res[dev])
        updated[dev] = re.sub("tag=\d+", f"tag={vlan}", updated[dev])
    async with proxmox.post(
        f"/api2/json/nodes/{settings.PROXMOX_NODE}/qemu/{new_id}/config",
        json=updated,
    ) as _:
        pass


@router.post("", response_model=EnvironmentOut, status_code=status.HTTP_201_CREATED)
async def create_environment(
    environment: EnvironmentIn, auth=Depends(EitherCheck), proxmox=Depends(Proxmox)
):
    if "Administrator" not in auth.roles:
        existing = await fetch_environments(
            bundle_id=str(environment.bundle), auth=auth, auto_error=False
        )
        if existing:
            return Response(
                status_code=status.HTTP_409_CONFLICT,
                content="Environment for this bundle already exists",
            )

    bundle = await validate_bundle(environment.bundle, auth)

    await create_user(proxmox, auth.username)

    vlan = await new_vlan()
    vmids = []

    async with TaskManager() as manager:
        for association in bundle["templates"]:
            if not isinstance(association["template_id"], UUID):
                association["template_id"] = UUID(association["template_id"])
            t = await read_template(association["template_id"])
            new_id = await new_vmid()
            vmids.append(new_id)
            manager.add(clone_vm(proxmox, t, new_id))

    for i, association in enumerate(bundle["templates"]):
        await set_perms(proxmox, association, auth.username, vmids[i])
        await update_network(proxmox, vmids[i], vlan)

    try:
        result = await collection.insert_one(
            {
                **jsonable_encoder(environment),
                "id": uuid4(),
                "state": "STOPPED",
                "vmids": vmids,
                "vlan": vlan,
                "owner": auth.username,
            }
        )
        return await collection.find_one({"_id": result.inserted_id})
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to create DB entry"
        )


@router.get("", response_model=List[EnvironmentOut])
async def read_environments(
    auth: Auth = Depends(EitherCheck),
    bundle: str = None,
    owner: str = None,
    deleted: bool = False,
    state: str = None,
):
    return await fetch_environments(
        auto_error=False, auth=auth, bundle_id=bundle, owner=owner, deleted=deleted, state=state
    )


@router.get("/{environment_id}", response_model=EnvironmentOut)
async def read_environment(environment_id: UUID, auth=Depends(EitherCheck), deleted=False):
    return (await fetch_environments(environment_id=environment_id, auth=auth, deleted=deleted))[0]


async def update_environment(environment_id: UUID, environment: EnvironmentUpdate, auth):
    # deleted=True here, so we can support updating _any_ environment as an admin
    await fetch_environments(environment_id=environment_id, auth=auth, deleted=True)

    try:
        return await collection.find_one_and_update(
            {"id": environment_id},
            {"$set": jsonable_encoder(environment.dict(exclude_none=True))},
            return_document=True,
        )
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to update DB entry"
        )


async def delete_task(proxmox, mongo, auth, environment_id):
    environment = (await fetch_environments(environment_id=environment_id, auth=auth))[0]
    await update_environment(environment_id=environment_id, environment=EnvironmentUpdate(state="DELETING"), auth=auth)

    async with TaskManager() as manager:
        async with proxmox.get("/api2/json/access/acl") as resp:
            acl = (await resp.json())["data"]

        for vmid in environment.get("vmids", []):
            for rule in acl:
                if rule["path"] == f"/vms/{vmid}":
                    manager.add(
                        proxmox.put(
                            "/api2/json/access/acl",
                            json={
                                "path": f"/vms/{vmid}",
                                "roles": rule["roleid"],
                                "users": [rule["ugid"]],
                                "delete": True,
                            },
                        )
                    )
            async with proxmox.post(
                f"/api2/json/nodes/{settings.PROXMOX_NODE}/qemu/{vmid}/status/stop"
            ) as resp:
                upid = (await resp.json())["data"]
                manager.add(proxmox.wait(upid))

    async with TaskManager() as manager:
        async with proxmox.delete(
            f"/api2/json/nodes/{settings.PROXMOX_NODE}/qemu/{vmid}",
            params={"destroy-unreferenced-disks": 1, "purge": 1},
        ) as resp:
            upid = (await resp.json())["data"]
            manager.add(proxmox.wait(upid))

    await mongo.environments.update_one(
        {"id": environment_id}, {"$set": {"vmids": [], "deleted": True}}
    )

@router.delete("/{environment_id}", status_code=status.HTTP_200_OK)
async def delete_environment(
    environment_id: UUID,
    background_tasks: BackgroundTasks,
    auth=Depends(EitherCheck),
    proxmox=Depends(Proxmox),
    mongo=Depends(MongoDB),
):
    background_tasks.add_task(delete_task, proxmox, mongo, auth, environment_id)


@router.get("/{environment_id}/tasks", status_code=status.HTTP_200_OK)
async def get_environment_tasks(
    environment_id: UUID, auth=Depends(EitherCheck), proxmox=Depends(Proxmox)
):
    environment = (await fetch_environments(environment_id=environment_id, auth=auth))[0]
    node = settings.PROXMOX_NODE
    tasks = []

    for vmid in environment["vmids"]:
        async with proxmox.get(
            f"/api2/json/nodes/{node}/tasks", params={"vmid": vmid, "source": "active"}
        ) as resp:
            tasks.extend((await resp.json())["data"])

    return tasks


@router.put("/{environment_id}/start", status_code=status.HTTP_200_OK)
async def start_enviroment(
    environment_id: UUID, auth=Depends(EitherCheck), proxmox=Depends(Proxmox)
):
    environment = (await fetch_environments(environment_id=environment_id, auth=auth))[0]

    node = settings.PROXMOX_NODE
    async with TaskManager() as manager:
        for vmid in environment["vmids"]:
            manager.add(proxmox.post(f"/api2/json/nodes/{node}/qemu/{vmid}/status/start"))

    ret = await update_environment(
        environment_id=environment_id,
        environment=EnvironmentUpdate(state="RUNNING"),
        auth=auth,
    )
    return EnvironmentOut(**ret)


@router.put("/{environment_id}/stop", status_code=status.HTTP_200_OK)
async def stop_enviroment(
    environment_id: UUID,
    auth=Depends(EitherCheck),
    proxmox=Depends(Proxmox),
):
    environment = (await fetch_environments(environment_id=environment_id, auth=auth))[0]

    node = settings.PROXMOX_NODE
    async with TaskManager() as manager:
        for vmid in environment["vmids"]:
            manager.add(
                proxmox.post(
                    f"/api2/json/nodes/{node}/qemu/{vmid}/status/shutdown", json={"forceStop": 1}
                )
            )

    ret = await update_environment(
        environment_id=environment_id,
        environment=EnvironmentUpdate(state="STOPPED"),
        auth=auth,
    )
    return EnvironmentOut(**ret)
