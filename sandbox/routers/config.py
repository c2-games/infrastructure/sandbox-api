from fastapi import APIRouter
from pydantic import BaseModel

from sandbox.shared import settings

router = APIRouter(prefix="/config", tags=["Config"])


class ProxmoxConfig(BaseModel):
    url: str
    pool: str
    node: str

    class Config:
        json_schema_extra = {"example": {"description": "An Example Job"}}


@router.get("/proxmox", response_model=ProxmoxConfig)
async def get_health():
    return ProxmoxConfig(
        url=settings.PROXMOX_URL,
        pool=settings.PROXMOX_POOL,
        node=settings.PROXMOX_NODE,
    )
