from datetime import datetime
from typing import List, Optional
from uuid import UUID, uuid4

from fastapi import APIRouter, Depends, HTTPException, Response, status
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, Field
from pymongo.errors import PyMongoError

from ..proxmox import old_proxmox
from ..security import AdminCheck, Auth
from ..shared import db, settings, Proxmox

router = APIRouter(prefix="/templates", tags=["Templates"])

collection = db.templates


class TemplateBase(BaseModel):
    name: str
    vmid: int
    description: Optional[str] = None
    modified: datetime = Field(default_factory=datetime.now)

    class Config:
        json_schema_extra = {
            "example": {
                "name": "Example",
                "description": "An Example Template",
                "vmid": 1000,
            }
        }


class TemplateIn(TemplateBase):
    created: datetime = Field(default_factory=datetime.now)


class TemplateOut(TemplateBase):
    id: UUID
    # default for templates created before a created/modified time was stored
    created: datetime = None

    class Config:
        json_schema_extra = {
            "example": {
                "id": uuid4(),
                "name": "Example",
                "description": "An Example Template",
                "vmid": 1000,
            }
        }


class TemplateUpdate(TemplateBase):
    name: Optional[str] = None
    vmid: Optional[int] = None
    description: Optional[str] = None
    modified: datetime = Field(default_factory=datetime.now)

    class Config:
        json_schema_extra = {
            "example": {
                "name": "Optional New Name",
                "description": "Optional New Description",
                "vmid": 1234,
            }
        }


async def fetch_templates(
    template_id: UUID = None,
    auto_error: bool = True,
    name: str = None,
    vmid: int = None,
) -> List[TemplateOut]:
    query = {}

    if template_id:
        query["id"] = template_id
    if name:
        query["name"] = name
    if vmid:
        query["vmid"] = vmid

    try:
        result = [template async for template in collection.find(query)]
        if auto_error and len(result) == 0:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Template not found")
    except PyMongoError:
        raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to read DB")

    return result


async def validate_template(proxmox, vmid):
    if not any(
        vm.get("vmid", 0) == vmid
        # Check if the supplied VM ID is
        # 1: in the configured pool
        for vm in old_proxmox.pools[settings.PROXMOX_POOL]
        # 2: if the VM is a PVE template
        if vm.get("template", False)
    ):
        raise HTTPException(
            status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"VMID {vmid} not found in Pool",
        )


@router.post("", response_model=TemplateOut, status_code=status.HTTP_201_CREATED)
async def create_template(template: TemplateIn, auth: Auth = Depends(AdminCheck), proxmox = Depends(Proxmox)):
    await validate_template(proxmox, template.vmid)

    try:
        result = await collection.insert_one(
            {"id": uuid4(), "creator": auth.username, **jsonable_encoder(template)}
        )
        return await collection.find_one({"_id": result.inserted_id})
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to create DB entry"
        )


@router.get("", response_model=List[TemplateOut])
async def read_templates(name: str = None, vmid: int = None):
    return await fetch_templates(auto_error=False, name=name, vmid=vmid)


@router.get("/{template_id}", response_model=TemplateOut)
async def read_template(template_id: UUID):
    return (await fetch_templates(template_id=template_id))[0]


@router.patch("/{template_id}", response_model=TemplateOut)
async def update_template(template_id: UUID, template: TemplateUpdate, proxmox=Depends(Proxmox)):
    await fetch_templates(template_id=template_id)

    if template.vmid:
        await validate_template(proxmox, template.vmid)

    try:
        return await collection.find_one_and_update(
            {"id": template_id},
            {"$set": jsonable_encoder(template.dict(exclude_none=True))},
            return_document=True,
        )
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to update DB entry"
        )


@router.delete("/{template_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_template(template_id: UUID):
    await fetch_templates(template_id=template_id)

    try:
        await collection.delete_one({"id": template_id})
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to delete DB entry"
        )

    return Response(status_code=status.HTTP_204_NO_CONTENT)
