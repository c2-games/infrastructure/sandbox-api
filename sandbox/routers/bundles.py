from datetime import datetime
from typing import List, Optional
from uuid import UUID, uuid4

from fastapi import APIRouter, Depends, HTTPException, Response, status
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, Field
from pymongo.errors import PyMongoError

from ..security import AdminCheck, Auth, EitherCheck
from ..shared import db
from .templates import read_template

router = APIRouter(prefix="/bundles", tags=["Bundles"])

collection = db.bundles


class TemplateAssociation(BaseModel):
    template_id: UUID
    user_role: str


class BundleBase(BaseModel):
    name: str
    description: Optional[str] = None
    how_to_play: Optional[str] = None
    templates: List[TemplateAssociation]
    # default to false for admin testing before release
    active: bool = False
    modified: datetime = Field(default_factory=datetime.now)

    class Config:
        json_schema_extra = {
            "example": {
                "name": "Example",
                "description": "An Example Bundle",
                "templates": [{"template_id": uuid4(), "user_role": "PVEVMUser"}],
            }
        }


class BundleIn(BundleBase):
    created: datetime = Field(default_factory=datetime.now)


class BundleOut(BundleBase):
    id: UUID
    created: datetime = None

    class Config:
        json_schema_extra = {
            "example": {
                "id": uuid4(),
                "name": "Example",
                "description": "An Example Bundle",
                "templates": [{"template_id": uuid4(), "user_role": "PVEVMUser"}],
            }
        }


class BundleUpdate(BundleBase):
    name: Optional[str] = None
    templates: Optional[List[TemplateAssociation]] = None
    description: Optional[str] = None
    active: Optional[bool] = None
    modified: datetime = Field(default_factory=datetime.now)


async def fetch_bundles(
    auto_error: bool = True,
    bundle_id: UUID = None,
    name: str = None,
    inactive: bool = False,
) -> List[BundleOut]:
    query = {}

    if name:
        query["name"] = name
    if bundle_id:
        query["id"] = bundle_id
    if not inactive:
        query["active"] = True

    try:
        result = [bundle async for bundle in collection.find(query)]
        if auto_error and len(result) == 0:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Bundle not found")
    except PyMongoError:
        raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to read DB")

    return result


async def validate_associations(bundle):
    for association in bundle.templates:
        try:
            await read_template(association.template_id)
        except HTTPException as e:
            if e.status_code == 404:
                raise HTTPException(
                    status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    detail=f"Invalid template_id: {association.template_id}",
                )
            else:
                raise e


@router.post("", response_model=BundleOut, status_code=status.HTTP_201_CREATED)
async def create_bundle(bundle: BundleIn, auth: Auth = Depends(AdminCheck)):
    await validate_associations(bundle)

    try:
        result = await collection.insert_one(
            {"id": uuid4(), "creator": auth.username, **jsonable_encoder(bundle)}
        )
        return await collection.find_one({"_id": result.inserted_id})
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to create DB entry"
        )


@router.get("", response_model=List[BundleOut])
async def read_bundles(auth=Depends(EitherCheck)):
    return await fetch_bundles(auto_error=False, inactive=auth.is_admin)


@router.get("/{bundle_id}", response_model=BundleOut)
async def read_bundle(bundle_id: UUID, auth=Depends(EitherCheck)):
    return (await fetch_bundles(bundle_id=bundle_id, inactive=auth.is_admin))[0]


@router.patch("/{bundle_id}", response_model=BundleOut)
async def update_bundle(bundle_id: UUID, bundle: BundleUpdate, auth=Depends(AdminCheck)):
    await fetch_bundles(bundle_id=bundle_id, inactive=auth.is_admin)

    if bundle.templates:
        await validate_associations(bundle)

    try:
        return await collection.find_one_and_update(
            {"id": bundle_id},
            {"$set": jsonable_encoder(bundle.dict(exclude_none=True))},
            return_document=True,
        )
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to update DB entry"
        )


@router.delete("/{bundle_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_bundle(bundle_id: UUID, auth=Depends(AdminCheck)):
    await fetch_bundles(bundle_id=bundle_id, inactive=auth.is_admin)

    try:
        await collection.delete_one({"id": bundle_id})
    except PyMongoError:
        raise HTTPException(
            status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to delete DB entry"
        )

    return Response(status_code=status.HTTP_204_NO_CONTENT)
