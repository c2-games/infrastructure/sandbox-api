from typing import List, Optional

import jwt
import requests
from fastapi import Depends, HTTPException, status
from fastapi.security import (
    APIKeyHeader,
    HTTPAuthorizationCredentials,
    HTTPBearer,
    OpenIdConnect,
)
from pydantic import BaseModel
from starlette.requests import Request

from .shared import db, settings

collection = db.users


class Auth(BaseModel):
    username: str
    roles: List[str]
    type: str

    @property
    def is_admin(self):
        return "Administrator" in self.roles

    class Config:
        json_schema_extra = {
            "example": {
                "username": "sandbox@ncaecybergames.org",
                "role": ["Administrator"],
                "type": "APIKey",
            }
        }


class APIKey(APIKeyHeader):
    async def __call__(self, request: Request) -> Auth:
        try:
            api_key: str = await super().__call__(request=request)
        except HTTPException:
            api_key = None
        if not api_key:
            if self.auto_error:
                raise HTTPException(status.HTTP_401_UNAUTHORIZED, detail="No API Key Provided")
            return "MISSING"

        result = await collection.find_one({"key": api_key})
        if not result:
            if self.auto_error:
                raise HTTPException(status.HTTP_403_FORBIDDEN, detail="Access Denied")
            return "DENIED"

        return Auth(
            username=result["username"],
            roles=result["roles"],
            type="X-API-KEY",
        )


bearer_scheme = HTTPBearer(auto_error=False)

header_scheme = APIKey(name="X-API-KEY")
manual_header = APIKey(name="X-API-KEY", auto_error=False)


class OpenID(OpenIdConnect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        openid_config = requests.get(settings.WELL_KNOWN_URL).json()
        self.jwks_client = jwt.PyJWKClient(openid_config["jwks_uri"], cache_keys=True)

    async def __call__(self, request: Request) -> Optional[str]:
        authorization = await super().__call__(request)
        if authorization is None:
            return "MISSING"
        encoded = authorization.split(" ")[1]
        signing_key = self.jwks_client.get_signing_key_from_jwt(encoded)

        try:
            token = jwt.decode(
                encoded,
                signing_key.key,
                algorithms=["RS256"],
                audience=settings.OAUTH_CLIENT,
            )
        except jwt.InvalidTokenError as e:
            print("JWT access denied:", e)
            return "DENIED"

        auth = Auth(
            username=token["email"],
            type="openid",
            roles=token["resource_access"].get(settings.OAUTH_CLIENT, {}).get("roles", []),
        )
        return auth


openid_scheme = OpenID(
    openIdConnectUrl=settings.WELL_KNOWN_URL,
    auto_error=False,
)


async def EitherCheck(
    key: Auth = Depends(manual_header),
    openid: Auth = Depends(openid_scheme),
) -> Auth:
    methods = [key, openid]
    if any(method == "DENIED" for method in methods):
        raise HTTPException(status.HTTP_403_FORBIDDEN)
    if all(method == "MISSING" for method in methods):
        raise HTTPException(status.HTTP_401_UNAUTHORIZED)

    for method in methods:
        if isinstance(method, Auth):
            return method

    raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR)


async def AdminCheck(auth: Auth = Depends(EitherCheck)) -> Auth:
    if not auth.is_admin:
        raise HTTPException(status.HTTP_403_FORBIDDEN)
    return auth
