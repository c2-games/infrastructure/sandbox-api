import asyncio
from asyncio import sleep
from time import time

from aiohttp import ClientSession
from fastapi import HTTPException, Request, status
from motor.motor_asyncio import AsyncIOMotorClient
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    MONGO_HOST: str = "127.0.0.1"
    MONGO_DB: str = "sandbox"
    PROXMOX_URL: str
    PROXMOX_TOKEN: str
    PROXMOX_POOL: str
    PROXMOX_REALM: str
    PROXMOX_NODE: str
    PROXMOX_START_ID: int
    PROXMOX_END_ID: int
    PROXMOX_BRIDGE: str
    OAUTH_CLIENT: str
    LISTEN_PORT: int = 8000
    RELOAD: bool = False
    WELL_KNOWN_URL: str

    model_config = SettingsConfigDict(env_file=".env", extra="ignore")


class TaskManager:
    async def __aenter__(self):
        self._tasks = list()
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await asyncio.gather(*self._tasks)

    def add(self, task):
        self._tasks.append(task)


settings = Settings()

mongo_client = AsyncIOMotorClient(settings.MONGO_HOST, uuidRepresentation="pythonLegacy")
mongo_client.get_io_loop = asyncio.get_running_loop

db = mongo_client[settings.MONGO_DB]


async def MongoDB(request: Request):
    return request.app.mongodb


class ProxmoxSession(ClientSession):
    def __init__(self):
        super().__init__(raise_for_status=True, base_url=settings.PROXMOX_URL)
        self.headers.update({"Authorization": "PVEAPIToken=" + settings.PROXMOX_TOKEN})

    async def wait(self, upid: str, timeout: int = 30):
        start_time = time()
        while time() < start_time + timeout:
            async with self.get(
                f"/api2/json/nodes/{settings.PROXMOX_NODE}/tasks/{upid}/status"
            ) as resp:
                exitstatus = (await resp.json())["data"].get("exitstatus")
            if exitstatus == "OK":
                break
            elif exitstatus is None:
                await sleep(0.25)
            else:
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)


async def Proxmox(request: Request) -> ProxmoxSession:
    return request.app.proxmox
