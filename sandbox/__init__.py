import asyncio
from contextlib import asynccontextmanager

import uvicorn
from fastapi import Depends, FastAPI
from motor.motor_asyncio import AsyncIOMotorClient
from starlette.middleware.cors import CORSMiddleware

from .routers import bundles, config, environments, templates
from .security import AdminCheck, EitherCheck
from .shared import ProxmoxSession, settings


@asynccontextmanager
async def lifespan(app: FastAPI):
    async with ProxmoxSession() as proxmox:
        app.proxmox = proxmox
        mongo_client = AsyncIOMotorClient(settings.MONGO_HOST, uuidRepresentation="pythonLegacy")
        app.mongodb = mongo_client[settings.MONGO_DB]
        yield


app = FastAPI(title="Sandbox API", version="0.9.0", lifespan=lifespan)
app.include_router(templates.router, dependencies=[Depends(AdminCheck)])
app.include_router(bundles.router, dependencies=[Depends(EitherCheck)])
app.include_router(environments.router, dependencies=[Depends(EitherCheck)])
app.include_router(config.router, dependencies=[Depends(EitherCheck)])


@app.get("/status", tags=["Status"])
async def status():
    return {"status_code": 200, "message": "OK"}


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)


def main():
    uvicorn.run("sandbox:app", host="0.0.0.0", port=settings.LISTEN_PORT, reload=settings.RELOAD)
